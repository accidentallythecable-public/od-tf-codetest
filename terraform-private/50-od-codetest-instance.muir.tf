
# ID for AMI that provisioned systems use
variable "muir_ami_id" {
	type = string
	description = "AMI Id for muir"
}

# AWS EC2 instance size
variable "muir_instance_size" {
	type = string
	description = "EC2 Instance Size for muir"
}

# Name appended to instances for organization and recognizablity
variable "muir_instance_name_suffix" {
	type = string
	description = "Instance Name suffix for muir"
}

# Public/Private subnet placement
variable "muir_instance_visibility" {
	type = string
	description = "Visibility (public vs private subnet); specify 'public' or 'private'"
}

# SSH Key Pair name
variable "muir_key_name" {
	type = string
	description = "Key name to associate with instance"
}

resource "aws_instance" "muir" {
	ami = var.muir_ami_id
	associate_public_ip_address = true
	disable_api_termination = false
	ebs_optimized = false
	get_password_data = false
	instance_type = var.muir_instance_size
	key_name = var.muir_key_name
	monitoring = false
	source_dest_check = true
	subnet_id = var.muir_instance_visibility == "public" ? tolist(data.aws_subnet_ids.od-codetest-vpc_public.ids)[0] : tolist(data.aws_subnet_ids.od-codetest-vpc_private.ids)[0]
	tags = {
		Name = "od-codetest-instance.${var.muir_instance_name_suffix}"
		Managed-By = "Terraform/muir"
	}
	tenancy = "default"
	volume_tags = {
		"Name" = "muir"
	}
	vpc_security_group_ids = data.aws_security_groups.od-codetest-sg.ids

	credit_specification {
		cpu_credits = "standard"
	}

	root_block_device {
		delete_on_termination = true
		volume_size = 20
		volume_type = "gp2"
	}

# Send sl.deb to instance
	provisioner "file" {
		source = "../sl_1.0-1_amd64.deb"
		destination = "/tmp/sl.deb"
		connection {
			type = "ssh"
			user = "admin"
			private_key = file("/home/${var.admin_user}/.ssh/${var.muir_key_name}")
			host = self.public_ip
		}
	}

# Send todo-start.sh to instance
	provisioner "file" {
		source = "../todo-start.sh"
		destination = "/tmp/todo-start.sh"
		connection {
			type = "ssh"
			user = "admin"
			private_key = file("/home/${var.admin_user}/.ssh/${var.muir_key_name}")
			host = self.public_ip
		}
	}

# Send todo.tgz to instance
	provisioner "file" {
		source = "../todo.tgz"
		destination = "/tmp/todo.tgz"
		connection {
			type = "ssh"
			user = "admin"
			private_key = file("/home/${var.admin_user}/.ssh/${var.muir_key_name}")
			host = self.public_ip
		}
	}

# Install/Setup:
# install sl.deb, add nodejs + npm, use npm to install required base for todo app
	provisioner "remote-exec" {
		inline = [
			"sudo dpkg -i /tmp/sl.deb",
			"sudo apt-get update",
			"sudo apt-get -y install nodejs npm",
			"sudo npm install -g pm2 react serve yarn react-scripts --save",
			"sudo mkdir -p /var/www",
			"sudo useradd -m -d /var/www/todo -U -s /bin/nologin react"
		]
		connection {
			type = "ssh"
			user = "admin"
			private_key = file("/home/${var.admin_user}/.ssh/${var.muir_key_name}")
			host = self.public_ip
		}
	}
# Configure/Build:
# Unpack todo app, then build and launch
	provisioner "remote-exec" {
		inline = [
			"cd /var/www/todo",
			"sudo cp /tmp/todo-start.sh /var/www/todo/",
			"sudo chmod +x /var/www/todo/todo-start.sh",
			"sudo -u react -- tar -zxvf /tmp/todo.tgz",
			"sudo chown react:react -R /var/www/todo",
			"sudo -u react -- yarn add react",
			"sudo -u react -- yarn build",
			"sudo -u react -- pm2 start todo-start.sh"
		]
		connection {
			type = "ssh"
			user = "admin"
			private_key = file("/home/${var.admin_user}/.ssh/${var.muir_key_name}")
			host = self.public_ip
		}
	}

}
