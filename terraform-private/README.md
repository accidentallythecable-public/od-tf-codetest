# Opendrives Codetest - Chris Walker

##Instructions

After Cloning the repo;

- Edit vars.tfvars.json, modify the admin_ip (set to your IP or a bastion IP)
- Run ./bin/install.sh
	- This will install ruby, bundler, and terraform, as well as required ruby gems for create/delete scripts
- Create an EC2 Instance - ./bin/create.sh --help
	- `terraform init` may be required after this, for the first creation
- Remove an EC2 Instance - ./bin/delete.sh --help
- Update AWS with the current setup - ./bin/up.sh
- Teardown everything - ./bin/down.sh
- Show Instances - ./bin/show_instances.sh
