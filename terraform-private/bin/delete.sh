#!/usr/bin/env ruby

require "getoptlong"
require "json"

$baseDir = "#{File.dirname(File.dirname(__FILE__))}"

$varsFile = "#{$baseDir}/vars.tfvars.json"

opts = GetoptLong.new(
	[ '--help', '-h', GetoptLong::NO_ARGUMENT ],
	[ '--name', '-n', GetoptLong::REQUIRED_ARGUMENT ],
)

# Set Defaults
instName = nil

# help menu
def printhelp()
	puts("Remove EC2 Instance(s)\n")
	puts("--help\t\t\t-h\tThis Help Menu")
	puts("--name\t\t\t-n\tInstance Name Suffix (Instance Name)")
	exit 1
end

# Kick to help if no args
if ARGV.length == 0
	printhelp()
end

# Arg processing
opts.each do |opt,arg|
	case opt
		when '--help'
			printhelp()
			exit 1
		when '--name'
			instName = arg
	end
end

# Check to make sure all options are set
if instName.nil?
	puts("--name is required\n")
	printhelp()
	exit 1
end

# Output file name
instanceFName = "50-od-codetest-instance.#{instName}.tf"

# Halt if instance doesnt exist
if !File.exist?(instanceFName)
	puts("Instance: #{instName} does not exist?")
	exit 1
end

# Instance deletion and cleanup of config vars for vars.tfvars.json

File.delete(instanceFName)

currentVars = JSON.parse(File.read($varsFile))

currentVars.each do |k,v|
	if k =~ /^#{instName}_/
		currentVars.delete(k)
	end
end

newVarFile = File.open($varsFile,"w+")
newVarFile.write(JSON.pretty_generate(currentVars))
newVarFile.close

puts("Run #{$baseDir}/bin/up.sh")
