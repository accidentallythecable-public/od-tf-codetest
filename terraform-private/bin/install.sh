#!/usr/bin/env bash

# directory vars and temp dir creation
BASEDIR=$(dirname $(dirname $0))
TMPDIR=${BASEDIR}/tmp
if [ ! -d $TMPDIR ]; then
	mkdir $TMPDIR
fi

TERRA=$(dirname $0)/terraform


# Check if user has aws config setup, halt if not.

if [ ! -f ~/.aws/config ] && [ ! -f ~/.aws/credentials ]; then
	echo "Installation cannot continue, please setup your AWS credentials. Instructions here: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html"
	exit 1
fi

# Check if terraform exists somewhere on the system, and if not, install it.
if [ "$(which terraform)" == "" ]; then
	if [ ! -f $TERRA ]; then
		c=$(pwd)
		echo "Installing Terraform..."
		cd $TMPDIR;
		wget -O terraform.zip https://releases.hashicorp.com/terraform/0.14.5/terraform_0.14.5_linux_amd64.zip --no-check-certificate
		unzip terraform.zip
		mv terraform ../bin/
		cd $c
		if [ ! -f $TERRA ]; then
			echo "Installation failed."
			exit 1
		fi
		echo "Terraform Install success."
	fi
fi

# Check if ruby exists somewhere on the system, and if not, install it.
if [ "$(which ruby)" == "" ]; then
	echo "Installation requires Ruby, this will install os packages"
	if [ "$(egrep -i 'Debian|Ubuntu' /etc/*release*)" != "" ]; then
		c="sudo apt-get -y install ruby ruby-bundler"
	elif [ "$(egrep -i 'Centos|RedHat' /etc/*release*)" != "" ]; then
		c="sudo yum install -y ruby rubygem-bundler"
	fi
	echo "Will run: '$c', giving you 5 seconds to abort"
	sleep 5
	$($c)
fi

# Secondary check to ensure ruby actually installed.
if [ "$(which bundle)" == "" ]; then
	echo "Failed to install ruby / ruby bundler"
	exit 1
fi
echo "Any errors reported below may be fixed via sudo gem update --system --no-document; run this script again afterwards"

# Install ruby gems for create.sh/delete.sh
bundle install

# Init terraform, make sure all modules/providers are downloaded.
terraform init

# Initial provision
# od-codetest-vpc-data and od-codetest-secgroups have to be implemented after
#  the initial update because they reference things (VPC) that doesnt exist
#  yet. typically this isnt required, but terraform `depends_on` was not working
#  correctly.
# Second provision / update is run after renaming to get those into the environment
./bin/up.sh
mv 03-od-codetest-vpc_data.tf.inst 03-od-codetest-vpc_data.tf
mv 10-od-codetest-secgroups.tf.inst 10-od-codetest-secgroups.tf
./bin/up.sh
