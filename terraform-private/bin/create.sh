#!/usr/bin/env ruby

require "erb"
require "getoptlong"
require "json"

$baseDir = "#{File.dirname(File.dirname(__FILE__))}"
$templateFile = "#{$baseDir}/od-codetest-instance.template"

$varsFile = "#{$baseDir}/vars.tfvars.json"


opts = GetoptLong.new(
	[ '--help', '-h', GetoptLong::NO_ARGUMENT ],
	[ '--name', '-n', GetoptLong::REQUIRED_ARGUMENT ],
	[ '--ami', '-A', GetoptLong::REQUIRED_ARGUMENT ],
	[ '--size', '-s', GetoptLong::REQUIRED_ARGUMENT ],
	[ '--public', GetoptLong::NO_ARGUMENT ],
	[ '--private', GetoptLong::NO_ARGUMENT ],
	[ '--key', '-k', GetoptLong::REQUIRED_ARGUMENT ],
	[ '--template', '-T', GetoptLong::REQUIRED_ARGUMENT ]
)
#Set Defaults

instName = instAMI = instVis = instKey = nil

instSize = "t2.micro"

#help menu
def printhelp()
	puts("Create EC2 Instance(s)\n")
	puts("--help\t\t\t-h\tThis Help Menu")
	puts("--name\t\t\t-n\tInstance Name Suffix (Instance Name)")
	puts("--size\t\t\t-S\tInstance Size, must be a valid EC2 Instance Size, default: t2.micro")
	puts("--public\t\t\tMake Instance(s) Public")
	puts("--private\t\t\tMake Instance(s) Private")
	puts("--ami\t\t\t-A\tAWS AMI ID. AMI MUST BE AVAILABLE IN ACCOUNT BEFORE RUNNING")
	puts("--key\t\t\t-k\tKey Pair name to associate with instance(s). KEY PAIR MUST EXIST BEFORE RUNNING")
	puts("--template\t\t-T\tFile name for Template, default: #{$templateFile}")
	exit 1
end

#template rendering
# arg: fileName - path to template file
# arg: optionData - hash of template data to process
# return: nil on error, rendered template on success
def render_template(fileName,optionData)
	tplOpts = OpenStruct.new(optionData).instance_eval { binding }
	input = File.read(fileName)
	out = nil
	begin
		out = input
		until out !~ /\<%/
			e = ERB.new(out,nil,">")
			out = e.result(tplOpts)
		end
	rescue => error
		puts("Error parsing file: #{fileName}: #{error}")
		return nil
	end
	return out
end

# Kick to help if no args
if ARGV.length == 0
	printhelp()
end

# Arg processing
opts.each do |opt,arg|
	case opt
		when '--help'
			printhelp()
			exit 1
		when '--name'
			instName = arg
		when '--size'
			instSize = arg
		when '--key'
			instKey = arg
		when '--public'
			instVis = "public"
		when '--private'
			instVis = "private"
		when '--ami'
			instAMI = arg
		when '--template'
			if !File.exist?("#{$baseDir}/#{arg}")
				puts("Template: #{$baseDir}/#{arg} does not exist")
				printhelp()
			end
			$templateFile = arg
	end
end

# Check to make sure all options set
if instName.nil?
	puts("--name is required\n")
	printhelp()
	exit 1
elif instKey.nil?
	puts("--key is required\n")
	printhelp()
	exit 1
elif instAMI.nil?
	puts("--ami is required\n")
	printhelp()
	exit 1
elif instVis.nil?
	puts("--public or --private is required\n")
	printhelp()
	exit 1
end

templateName = instName.downcase
templateOptions = {
		"template": templateName,
}

varFileOptions = {
		"instance_name_suffix": instName,
		"key_name": instKey,
		"ami_id": instAMI,
		"instance_visibility": instVis,
		"instance_size": instSize
}

# Output file name
newInstanceFName = "#{$baseDir}/50-od-codetest-instance.#{templateName}.tf"

# Dont allow to continue if template already exists
if File.exist?(newInstanceFName)
	puts("Instance with name: #{templateName} is already configured")
	exit 1
end

# Rendering process, adding configuration options to vars.tfvars.json

newInstanceData = render_template($templateFile,templateOptions)

currentVars = JSON.parse(File.read($varsFile))

varFileOptions.each do |k,v|
	currentVars["#{templateName}_#{k}"] = v
end

puts("Writing new Terraform file: #{newInstanceFName}")
newInstanceFile = File.open(newInstanceFName,"w+")
newInstanceFile.write(newInstanceData)
newInstanceFile.close

newVarFile = File.open($varsFile,"w+")
newVarFile.write(JSON.pretty_generate(currentVars))
newVarFile.close

puts("Run #{$baseDir}/bin/up.sh")
