#!/usr/bin/env bash

if [ "$1" == "" ]; then
	echo "Type: $0 show _instance_ to display information about the instance" 
	terraform state list | grep "aws_instance" | cut -d . -f 2
elif [ "$1" == "show" ]; then
	if [ "$2" == "" ]; then
		echo "missing an instance name"
		exit 1
	fi
	terraform state show aws_instance.${2}
fi
