#!/usr/bin/env bash

terraform validate && terraform plan -var-file=vars.tfvars.json || exit 1
echo "Waiting 5 seconds for you to abort"
sleep 5
terraform apply -var-file=vars.tfvars.json
