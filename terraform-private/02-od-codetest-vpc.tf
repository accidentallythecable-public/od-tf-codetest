# VPC
# US-East
# 10.0.0.0/16
# 1 Elastic IP
# US-East-1A
# Public, Private

resource "aws_eip" "od-codetest-nat" {
	count = 1
	vpc = true
}

module "vpc" {
	source  = "terraform-aws-modules/vpc/aws"
	version = "2.68.0"

	name = "od-codetest-vpc"
	cidr = "10.0.0.0/16"

	azs = [ "us-east-1a" ]
	private_subnets = [ "10.0.1.0/24" ]
	public_subnets = [ "10.0.254.0/24" ]

	enable_nat_gateway = true

	tags = {
		Name = "od-codetest-vpc"
	}
	reuse_nat_ips = true
	single_nat_gateway = false
	external_nat_ip_ids = aws_eip.od-codetest-nat.*.id
	one_nat_gateway_per_az = true

	public_subnet_suffix = "-public"

	public_subnet_tags = {
		Visibility = "public"
	}
	private_subnet_suffix = "-private"
	private_subnet_tags = {
		Visibility = "private"
	}
}
