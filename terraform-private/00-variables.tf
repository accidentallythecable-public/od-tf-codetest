# Base Variables,

# IP Address which will be given SSH access.
variable "admin_ip" {
	type = string
	description = "Admin/bastion IP"
}

# User (local to this system) which the ssh key is located in /home/{admin_user}/.ssh/{key_name}
variable "admin_user" {
	type = string
	description = "Your local user, used to locate SSH Key(s) for provisioners"
}
